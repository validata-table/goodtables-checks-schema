# Changelog

## v0.2 -> v0.3

Breaking changes:

- Remove `pre_checks` property.

## v0.1 -> v0.2

Breaking changes:

- Rename `checks` property to `custom_checks`

Non-breaking changes:

- Add `pre_checks` property

## v0.1

- First version
