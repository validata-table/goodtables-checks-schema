# JSON schema for Goodtables checks

This repository defines a proposal where table-schema files can declare configuration for custom checks.

Custom checks are implemented in Goodtables as [Python modules](https://github.com/frictionlessdata/goodtables-py#how-can-i-add-a-new-custom-check). They are executed after a normal schema validation on the table cells.

Custom checks take parameters, like the name of the column(s) of the tabular file to be checked. The `custom_checks` property declares these parameters.
